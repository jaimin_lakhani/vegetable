/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author jaiminlakhani
 */
public class VegetableFactory {
    private static VegetableFactory factory;
    
    private VegetableFactory() {}
    
    public static VegetableFactory getInstance() {
        if(factory == null) {
            factory = new VegetableFactory();
        }
        return factory;
    }
    
    public Vegetable getVegetable(VegetableType type, String colour, double size) {
        switch(type) {
            case CARROT : return new Carrot(colour, size);
            
        }
        return null;
    }
    
}
