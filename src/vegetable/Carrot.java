package vegetable;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jaiminlakhani
 */
public class Carrot extends Vegetable {
    
    public Carrot(String colour, double size) {
        super(colour, size);
    }
    
    public boolean isRipe() {
        return getSize() >= 1.5 && getColour().equalsIgnoreCase("orange");
    }
}
