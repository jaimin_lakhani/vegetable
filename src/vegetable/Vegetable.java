/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vegetable;


/**
 *
 * @author jaiminlakhani
 */
abstract public class Vegetable {
    
    protected String colour;
    protected double size;
    
    protected Vegetable(String colour, double size) {
        this.colour = colour;
        this.size = size;
    }
    
    
    public String getColour() {
        return this.colour;
    }
    
    public double getSize() {
        return this.size;
    }
    
    //abstract boolean isTasty();
    
    abstract boolean isRipe();
 }
