/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author jaiminlakhani
 */
public class Tomato extends Vegetable {
    
    //constructor(s)
    public Tomato(String colour, double size) {
        super(colour, size);
    }
    
    //getters
    @Override    
    public double getSize() {return super.size;}
    public String getColour() {return super.colour;}    
    
    //method(s)
    @Override
    public boolean isRipe() {
        if(super.size == 4.5 && super.colour == "red"){
            return true;
        } else {
            return false;
        }
    }    
}
