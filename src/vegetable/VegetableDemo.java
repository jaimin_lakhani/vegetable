/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author jaiminlakhani
 */
public class VegetableDemo {
    public static void main(String[] args) {
        VegetableFactory factory = VegetableFactory.getInstance();
        
        Vegetable carrot = factory.getVegetable(VegetableType.CARROT, "ORANGE", 2);
        
        System.out.println("Colour of " +carrot.getClass().getCanonicalName()+" is " +carrot.getColour() + " and is it ripe ? " +carrot.isRipe());
    }
    
}
