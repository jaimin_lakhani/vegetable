package vegetable;

public enum VegetableType {
    BROCCOLI,
    SPINACH,
    TOMATO,
    CARROT,
    BEET
}