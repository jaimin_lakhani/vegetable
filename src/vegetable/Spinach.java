/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author jaiminlakhani
 */
public class Spinach extends Vegetable {
    
    //constructor(s)
    public Spinach(String colour, double size) {
        super(colour, size);
    }
    
    //getters
    @Override    
    public double getSize() {return super.size;}
    public String getColour() {return super.colour;}    
    
    //method(s)
    @Override
    public boolean isRipe() {
        if(super.size == 3.5 && super.colour == "light green"){
            return true;
        } else {
            return false;
        }
    }    
}
